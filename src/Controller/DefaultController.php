<?php

namespace App\Controller;

use App\Model\CreneauxModel;
use App\Model\SallesModel;
use App\Model\UsersModel;
use Core\Kernel\AbstractController;

/**
 *
 */
class DefaultController extends AbstractController
{
    public function index()
    {
        $salles = sallesModel::all();
        $users = UsersModel::all();
        $creneaux = creneauxModel::selectcreneau();
        $this->render('app.default.frontpage', array(
            'salles' => $salles,
            'users' => $users,
            'creneaux' => $creneaux,
        ));
    }

}
