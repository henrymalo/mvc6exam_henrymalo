<?php

namespace App\Controller;

use App\Model\UsersModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;
use Core\Kernel\View;

class UsersController extends AbstractController
{
    public function listingusers()
    {
        $users = UsersModel::all();
        //$this->dump($users);
        $this->render('app.default.listingusers', array(
            'users' => $users,
        ));
    }
    public function adduser()
    {
        $errors = $this->AbonneFormSubmission();
        $form = new Form($errors);
        $this->render('app.default.adduser', array(
            'form' => $form,
        ));
    }
    private function AbonneFormSubmission() {
        $errors = array();

        if(!empty($_POST['submitted'])){
            $post= $this->cleanXss($_POST);
            $validation = new Validation();
            $errors = $this->validate($validation, $post);
            if($validation->IsValid($errors)){
                UsersModel::insert($post);
                $this->addFlash('success', 'Merci pour avoir créé cet utilisateur!');
                $this->redirect('listingusers');
            }

        }

        return $errors;
    }

    private function validate($validation, $post)
    {
        $errors = [];
        $errors['nom'] = $validation->textValid($post['nom'], 'nom', 1, 100);
        $errors['email'] = $validation->emailValid($post['email']);
        return $errors;
    }
}
