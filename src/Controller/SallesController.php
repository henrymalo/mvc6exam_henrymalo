<?php

namespace App\Controller;

use App\Model\SallesModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;
use Core\Kernel\View;

class SallesController extends AbstractController
{
    public function listingsalles()
    {
        $salles = SallesModel::all();
        //$this->dump($salles);
        $this->render('app.default.listingsalles', array(
            'salles' => $salles,
        ));
    }
    public function addsalle()
    {
        $errors = $this->AbonneFormSubmission();
        $form = new Form($errors);
        $this->render('app.default.addsalle', array(
            'form' => $form,
        ));
    }
    private function AbonneFormSubmission() {
        $errors = array();

        if(!empty($_POST['submitted'])){
            $post= $this->cleanXss($_POST);
            $validation = new Validation();
            $errors = $this->validate($validation, $post);
            if($validation->IsValid($errors)){
                    sallesModel::insert($post);
                    $this->addFlash('success', 'Merci pour avoir créé cette salle!');
                $this->redirect('listingsalles');
                }

            }

        return $errors;
    }

    private function validate($validation, $post)
    {
        $errors = [];
        $errors['title'] = $validation->textValid($post['title'], 'title', 1, 100);
        $errors['maxuser'] = $validation->numberValid($post['maxuser'], 'maxuser');
        return $errors;
    }
}
