<?php

namespace App\Controller;

use App\Model\CreneauxModel;
use App\Model\CreneauxUserModel;
use App\Model\SallesModel;
use App\Model\UsersModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;
use Core\Kernel\View;

class CreneauxController extends AbstractController
{
    public function listingcreneaux()
    {
        $creneaux = CreneauxModel::selectcreneau();
        $salles = SallesModel::all();
        $this->dump($creneaux);
        $this->render('app.default.listingcreneaux', array(
            'creneaux' => $creneaux,
            'salles' => $salles,
        ));
    }
    public function addcreneau()
    {
        $errors = $this->CreneauFormSubmission();
        $salles = sallesModel::all();
        $form = new Form($errors);
        $this->render('app.default.addcreneau', array(
            'form' => $form,
            'salles' => $salles,

        ));
    }
    public function singlecreneau($id)
    {
   if (!empty($_POST['submitted'])) {
    $this->addNewUser($id);
   }
        $oneCreneau = $this->getCreneauByIdOr404($id);
        $this->dump($oneCreneau);
        $users = UsersModel::all();
        $salles = SallesModel::all();
        $usersin = CreneauxModel::selectusertocreneau($id);
        $form2 = new Form();
        $this->render('app.default.singlecreneau', array(

            'id' => $id,
            'oneCreneau' => $oneCreneau,
            'salles' => $salles,
            'users' => $users,
            'usersin' => $usersin,
            'form2' => $form2,
        ));
    }

    private function addNewUser($id)
    {
        $errors=[];
        $post = $this->cleanXss($_POST);
        $validation = new Validation();
        if ($validation->IsValid($errors)) {
            CreneauxModel::insertUser($post,$id);
            $this->addFlash('success', 'Merci d\'avoir ajouté cet utilisateur !');
        }
    }
    private function CreneauFormSubmission() {
        $errors = array();

        if(!empty($_POST['submitted'])){
            $post= $this->cleanXss($_POST);
            $validation = new Validation();
            $errors = $this->validate($validation, $post);
            if($validation->IsValid($errors)){
                creneauxModel::insert($post);
                $this->addFlash('success', 'Merci pour avoir créé ce créneau!');
                $this->redirect('listingcreneaux');
            }

        }

        return $errors;
    }

    private function validate($validation, $post)
    {
        $errors = [];
        $errors['id_salle'] = $validation->numberValid($post['id_salle'], 'id_salle');
        $errors['nbrehours'] = $validation->numberValid($post['nbrehours'], 'nbrehours');
        return $errors;
    }

    private function getCreneauByIdOr404($id)
    {
        $oneCreneau = CreneauxModel::singlecreneau($id);
        if(empty($oneCreneau)) {
            $this->Abort404();
        }
        return $oneCreneau;
    }

    public function userincreneaudelete($id)
    {
        $this->getuserincreneauByIdOr404($id);
        CreneauxUserModel::delete($id);
        $this->addFlash('success', 'Merci pour avoir effacé cet utilisateur!');
    }
    private function getuserincreneauByIdOr404($id)
    {
        $user = CreneauxUserModel::findById($id);
        if(empty($user)) {
            $this->Abort404();
        }
        return $user;
    }
}
