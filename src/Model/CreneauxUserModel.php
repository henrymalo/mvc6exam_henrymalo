<?php


namespace App\Model;

//use Core\App;
use Core\App;
use Core\Kernel\AbstractModel;

class CreneauxUserModel extends AbstractModel
{
    protected static $table = 'creneau';
    protected $id;
    protected $id_user;
    protected $id_creneau;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @return mixed
     */
    public function getIdCreneau()
    {
        return $this->id_creneau;
    }



}
