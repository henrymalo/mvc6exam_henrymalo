<?php


namespace App\Model;

//use Core\App;
use Core\App;
use Core\Kernel\AbstractModel;

class CreneauxModel extends AbstractModel
{
    protected static $table = 'creneau';
    protected $id;
    protected $id_salle;
    protected $nbrehours;
    protected $start_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdSalle()
    {
        return $this->id_salle;
    }

    /**
     * @return mixed
     */
    public function getNbrehours()
    {
        return $this->nbrehours;
    }

    /**
     * @return mixed
     */
    public function formattedStartAt()
    {
        return date_format(date_create($this->start_at), 'Y-m-d H:i:s');
    }








    public static function insertUser($post, $id) : void
    {
        App::getDatabase()->prepareInsert("INSERT INTO creneau_user (id_user, id_creneau, created_at) VALUES (?,?,NOW())", array($post['id_user'],$id));
    }

    public static function insert($post) : void
    {
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (id_salle, nbrehours, start_at) VALUES (?,?,?)", array($post['id_salle'],$post['nbrehours'],$post['start_at']));
    }

    public static function selectcreneau()
    {
        return App::getDatabase()->query("SELECT s.title, s.maxuser, c.start_at, c.nbrehours, c.id
                                            FROM salle s
                                            LEFT JOIN creneau c ON s.id = c.id_salle;
                                            ",get_called_class());
    }

    public static function singlecreneau($id)
    {
        return App::getDatabase()->query("SELECT s.title, s.maxuser, c.start_at, c.nbrehours, c.id
                                        FROM salle s
                                        LEFT JOIN creneau c ON s.id = c.id_salle
                                        WHERE c.id = $id
                                        ",get_called_class());
    }

    public static function selectusertocreneau($id)
    {
        return App::getDatabase()->query("SELECT u.nom, u.email, cu.id as cu_id
                                            FROM user u 
                                            INNER JOIN creneau_user cu ON u.id = cu.id_user 
                                            WHERE cu.id_creneau = $id
                                            ",get_called_class());
    }

}
