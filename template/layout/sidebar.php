
<div class="wrapsidebar">
    <div class="logo">
        <img src="<?= $view->asset('img/logo.png')?>" alt="Logo du site">
    </div>

    <div class="sidebarcontainer">
        <div id="mainmenu" class="menuitem">
            <a href="<?= $view->path('home'); ?>">Accueil</a>
        </div>

        <div class="dropdown">
            <button class="dropdown-toggle" type="button" id="abonneDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                Créneaux horraires
            </button>
            <ul class="dropdown-menu" aria-labelledby="abonneDropdown">
                <li><a class="dropdown-item" href="<?= $view->path('listingcreneaux'); ?>">Liste des créneaux</a></li>
                <li><a class="dropdown-item" href="<?= $view->path('addcreneau'); ?>">Ajouter un créneau</a></li>
            </ul>
        </div>

        <div class="dropdown">
            <button class="dropdown-toggle" type="button" id="userDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                Utilisateurs
            </button>
            <ul class="dropdown-menu" aria-labelledby="userDropdown">
                <li><a class="dropdown-item" href="<?= $view->path('listingusers'); ?>">Liste des utilisateur</a></li>
                <li><a class="dropdown-item" href="<?= $view->path('adduser'); ?>">Ajouter un utilisateur</a></li>
            </ul>
        </div>

        <div class="dropdown">
            <button class="dropdown-toggle" type="button" id="salleDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                Salles
            </button>
            <ul class="dropdown-menu" aria-labelledby="salleDropdown">
                <li><a class="dropdown-item" href="<?= $view->path('listingsalles'); ?>">Liste des salles</a></li>
                <li><a class="dropdown-item" href="<?= $view->path('addsalle'); ?>">Ajouter une salle</a></li>
                <ul>
        </div>

    </div>
</div>
