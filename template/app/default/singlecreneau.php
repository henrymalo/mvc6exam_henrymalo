<section id="linstingcreneaux" class="listing">
    <div class="wrap">
        <h1><?= $oneCreneau[0]->title ?></h1>

        <section id="borrows_add" class="form2">
            <div class="wrap">
                <form action="" method="POST">
                    <?= $form2->label('Utilisateur', 'user') ?>
                    <?= $form2->selectEntity('id_user', $users, 'nom'); ?>
                    <?= $form2->error('user') ?>
                    <div class="submit">
                        <?= $form2->submit('submitted', 'Ajouter') ?>
                    </div>
                </form>

                        <h1>Liste des utilisateurs inscrits</h1>
                        <table class="listing">
                            <thead class="infolisting">
                            <tr class="info">
                                <th>Nom</th>
                                <th>Email</th>
                                <th>Fonctions</th>
                            </tr>
                            </thead>
                            <tbody class="one">
                            <?php foreach ($usersin as $user): ?>
                                <tr class="infoone">
                                    <td><?= $user->nom ?></td>
                                    <td><?= $user->email ?></td>
                                    <td class="menulisting">
                                        <a class="btn" onclick=" return confirm('Voulez-vous effacer ?')" href="<?= $view->path('userincreneaudelete', array('id' => $user->cu_id)) ?>">Effacer</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
            </div>
        </section>

    </div>
</section>

