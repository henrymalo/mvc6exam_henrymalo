<section id="adduser" class="form">
    <div class="wrap">
        <form action="" method="post">
            <?= $form->label('nom', 'Nom')?>
            <?= $form->input('nom')?>
            <?= $form->error('nom')?>

            <?= $form->label('email', 'E-mail')?>
            <?= $form->input('email')?>
            <?= $form->error('email')?>


            <div class="submit">
                <?= $form->submit('submitted', 'Ajouter un utilisateur')?>
            </div>
        </form>
    </div>
</section>