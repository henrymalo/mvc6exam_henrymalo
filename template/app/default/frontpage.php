<section id="linstingusers" class="listing">
    <div class="wrap">
        <h1>Liste des utilisateurs</h1>
        <table class="listing">
            <thead class="infolisting">
            <tr class="info">
                <th>Nom</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody class="one">
            <?php foreach ($users as $user): ?>
                <tr class="infoone">
                    <td><?= $user->nom ?></td>
                    <td><?= $user->email ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</section>

<section id="linstingsalles" class="listing">
    <div class="wrap">
        <h1>Liste des salles</h1>
        <table class="listing">
            <thead class="infolisting">
            <tr class="info">
                <th>Nom</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody class="one">
            <?php foreach ($salles as $salle): ?>
                <tr class="infoone">
                    <td><?= $salle->title ?></td>
                    <td><?= $salle->maxuser ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</section>

<section id="linstingcreneaux" class="listing">
    <div class="wrap">
        <h1>Liste des créneaux</h1>
        <table class="listing">
            <thead class="infolisting">
            <tr class="info">
                <th>Nom de la salle</th>
                <th>Nombre d'heures</th>
                <th>Commence à :</th>
                <th>Fonction</th>
            </tr>
            </thead>
            <tbody class="one">
            <?php foreach ($creneaux as $creneau): ?>
                <tr class="infoone">
                    <td><?= $creneau->title ?></td>
                    <td><?= $creneau->nbrehours ?> heures</td>
                    <td><?php echo $creneau->formattedStartAt(); ?></td>
                    <td class="menulisting">
                        <a href="<?= $view->path('singlecreneau/'.$creneau->id) ?>">Détail</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</section>
