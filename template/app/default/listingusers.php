
<section id="linstingusers" class="listing">
    <div class="wrap">
        <h1>Liste des utilisateurs</h1>
        <table class="listing">
            <thead class="infolisting">
            <tr class="info">
                <th>Nom</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody class="one">
            <?php foreach ($users as $user): ?>
                <tr class="infoone">
                    <td><?= $user->nom ?></td>
                    <td><?= $user->email ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</section>