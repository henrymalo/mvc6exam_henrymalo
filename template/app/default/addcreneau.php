<section id="addcreneau" class="form">
    <div class="wrap">
        <form action="" method="POST">
            <?= $form->label('Salle', 'Salle :') ?>
            <?= $form->selectEntity('id_salle', $salles, 'title'); ?>
            <?= $form->error('salle') ?>

            <?= $form->label('Nombre d\'heures', 'nbrehours')?>
            <?= $form->input('nbrehours')?>
            <?= $form->error('nbrehours')?>

            <?= $form->label('Commence à :', 'start_at') ?>
            <?= $form->input('start_at','datetime', date('d/m/y H:i')); ?>
            <?= $form->error('start_at') ?>


            <div class="submit">
                <?= $form->submit('submitted', 'Ouvrir un créneau') ?>
            </div>
        </form>