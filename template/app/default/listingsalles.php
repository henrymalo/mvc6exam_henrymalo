
<section id="linstingsalles" class="listing">
    <div class="wrap">
        <h1>Liste des salles</h1>
        <table class="listing">
            <thead class="infolisting">
            <tr class="info">
                <th>Nom</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody class="one">
            <?php foreach ($salles as $salle): ?>
                <tr class="infoone">
                    <td><?= $salle->title ?></td>
                    <td><?= $salle->maxuser ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</section>
