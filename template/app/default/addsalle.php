<section id="adduser" class="form">
    <div class="wrap">
        <form action="" method="post">
            <?= $form->label('Nom', 'title')?>
            <?= $form->input('title')?>
            <?= $form->error('title')?>

            <?= $form->label('Nombre d\'utilisateurs maximum', 'maxuser')?>
            <?= $form->input('maxuser')?>
            <?= $form->error('maxuser')?>


            <div class="submit">
                <?= $form->submit('submitted', 'Ajouter une salle')?>
            </div>
        </form>
    </div>
</section>